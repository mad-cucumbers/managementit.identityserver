using IdentityServer4.AspNetIdentity;
using ManagementIt.Identity.Core.Models;
using ManagementIt.Identity.DataAccess.Data;
using ManagementIt.Identity.IdentityConfiguration;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace ManagementIt.Identity
{
    public class Startup
    {
        private IConfiguration _configuration { get; }

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AuthDbContext>(options =>
            {
                options.UseNpgsql(_configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value);
            });

            services.AddIdentity<AppUser, IdentityRole>(config =>
            {
                config.Password.RequiredLength = 4;
                config.Password.RequireDigit = false;
                config.Password.RequireNonAlphanumeric = false;
                config.Password.RequireUppercase = false;

            }).AddEntityFrameworkStores<AuthDbContext>().AddDefaultTokenProviders();
           
            services.AddIdentityServer(options => 
            {
                options.UserInteraction.LoginUrl = "/Auth/login-get";
            }).AddProfileService<ProfileService<AppUser>>()
              .AddAspNetIdentity<AppUser>()
              .AddInMemoryApiResources(Configuration.ApiResources)
              .AddInMemoryIdentityResources(Configuration.IdentityResources)
              .AddInMemoryApiScopes(Configuration.ApiScopes)
              .AddInMemoryClients(Configuration.Clients)
              .AddDeveloperSigningCredential();

            services.ConfigureApplicationCookie(config =>
            {
                config.Cookie.Name = CookieAuthenticationDefaults.AuthenticationScheme;
                config.LoginPath = "/Auth/Login";
                config.LogoutPath = "/Auth/Logout";
            });

            services.AddControllersWithViews();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseIdentityServer();
            app.UseCors(c => c.AllowAnyOrigin());
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
