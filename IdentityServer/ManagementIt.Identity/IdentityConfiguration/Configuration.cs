﻿using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace ManagementIt.Identity.IdentityConfiguration
{
    public class Configuration
    {
        public static IEnumerable<ApiScope> ApiScopes =>
            new List<ApiScope>
            {
                new ApiScope("ManagementIT.WebHost.Swagger", "Web API"),
                new ApiScope("ManagementIT.WebHost", "web api"),
                new ApiScope("Ui.Blazor", "BLAZOR UI")
            };

        public static IEnumerable<IdentityResource> IdentityResources =>
            new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };

        public static IEnumerable<ApiResource> ApiResources =>
            new List<ApiResource>
            {
                new ApiResource("ManagementIT.WebHost.Swagger", "Swagger")
                {
                    Scopes = { "ManagementIT.WebHost.Swagger" }
                }
            };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "api_swagger",
                    ClientSecrets = { new Secret("client_secret_swagger".ToSha256()) },
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedCorsOrigins = { "https://localhost:4001" },
                    AllowedScopes =
                    {
                        "ManagementIT.WebHost.Swagger",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },
                },
                
                new Client
                {
                    ClientId = "blazor_ui",
                    ClientSecrets = { new Secret("ui_secret".ToSha256()) },
                    AllowedGrantTypes = GrantTypes.Code,
                    AllowedCorsOrigins = { "http://localhost:36560" },
                    AllowedScopes =
                    {
                        "Ui.Blazor",
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },
                    RedirectUris = { "http://localhost:36560/signin-oidc" }
                },
                new Client
                {
                    ClientId = "spaBlazorClient",
                    ClientName = "SPA Blazor Client",

                    RequireClientSecret = false,
                    RequireConsent = false,

                    RedirectUris = new List<string>
                    {
                        $"https://localhost:2001/ApplicationsAdmin",
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        $"https://localhost:2001/ApplicationsAdmin",
                    },
                    AllowedCorsOrigins = new List<string>
                    {
                        $"https://localhost:2001",
                    },

                    AllowedGrantTypes = GrantTypes.Code,
                    AllowedScopes = { "openid", "profile", "email", "api" },

                    AllowOfflineAccess = true,
                    RefreshTokenUsage = TokenUsage.ReUse
                }
            };
    }
}
